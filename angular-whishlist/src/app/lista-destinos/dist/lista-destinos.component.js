"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ListaDestinosComponent = void 0;
var core_1 = require("@angular/core");
var destino_viaje_model_1 = require("./../models/destino-viaje.model");
var ListaDestinosComponent = /** @class */ (function () {
    function ListaDestinosComponent() {
        this.destinos = [];
    }
    ListaDestinosComponent.prototype.ngOnInit = function () {
    };
    ListaDestinosComponent.prototype.guardar = function (nombre, url) {
        this.destinos.push(new destino_viaje_model_1.DestinoViaje(nombre, url));
        return false;
    };
    ListaDestinosComponent = __decorate([
        core_1.Component({
            selector: 'app-lista-destinos',
            templateUrl: './lista-destinos.component.html',
            styleUrls: ['./lista-destinos.component.css']
        })
    ], ListaDestinosComponent);
    return ListaDestinosComponent;
}());
exports.ListaDestinosComponent = ListaDestinosComponent;
